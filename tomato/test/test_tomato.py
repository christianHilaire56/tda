from __future__ import print_function
from __future__ import absolute_import
import numpy as np
import matplotlib.pyplot as plt
from tomato.nbrdgraphs import RadiusGraph, KnnGraph
from tomato.tomato import ToMAToModel
from tomato.density import invDistMeasure, truncatedGaussian
from tomato.utils import cluster_metrics, predict_cluster
from mpl_toolkits.mplot3d import Axes3D
import time
import pickle
import os


cwd = os.getcwd()

# ################## Toy example ################################################
# ###############################################################################
# # Retrieve the data
# filepath = os.path.join(cwd, 'tomato/data/toy_example_w_density.txt')
# data = np.loadtxt(filepath)
# print('Data shape: ', data.shape)
#
# points= data[:,:-1]
# density = data[:,-1]
#
# plt.figure(figsize=(12,12))
# plt.scatter(points[:,0], points[:,1], alpha = 0.2)
# figpath = os.path.join(cwd, 'plots/toy_example.png')
# plt.savefig(figpath)
#
# # testing Graph function
# G = RadiusGraph(points, 0.25)
#
#
# # Instantiate model with the provided density function
# model = ToMAToModel(G, density)
#
# # retrieve prominences
# prominences = model.get_mode_prominences()
# print(prominences[:10])
#
#
# #plot persistence diagram
# figpath = os.path.join(cwd, 'plots/toy_example_pd_diagram.png')
# model.generate_persistence_diagram(figpath)
#
# # update threshold
# model.update_threshold(1000)
#
# # retrieve cluster labels
# labels = model.get_cluster_labels()
#
#
# # removing non-clustered points
# locs = ~np.isnan(labels)
# points2 = points[locs,:]
# labels2 = labels[locs]
#
# plt.figure(figsize=(12,12))
# plt.scatter(points2[:,0], points2[:,1], c = labels2)
# figpath = os.path.join(cwd, 'plots/toy_example_with_clusters.png')
# plt.savefig(figpath)
#
# # saving
# filename = 'toy_example_info.pkl'
# filename = os.path.join(cwd, filename)
# outfile = open(filename,'wb')
# pickle.dump(G.neighbors_list, outfile)
# pickle.dump(density, outfile)
# pickle.dump(labels, outfile)
# outfile.close()
#
# # testing out inference
# selected = np.random.permutation(len(points))[:30]
# points_selected = points[selected,:] + 1.5*np.random.randn(30,2)
# Probs, predicted_labels = predict_cluster(points_selected, points, model)
#
#
#
# ################################################################################
# ########### Generating an approximate density function using invDistMeasure ####
# density = invDistMeasure(points, 60)
#
# density = 100*density # scaling is optional
#
# print('Density shape: ', density.shape)
#
# model = ToMAToModel(G, density)
#
# #plot persistence diagram
# figpath = os.path.join(cwd, 'plots/toy_example_pd_diagram_from_computed_density.png')
# model.generate_persistence_diagram(figpath)
#
# # retrieve prominences
# prominences = model.get_mode_prominences()
# print(prominences[:10])
#
#
# # update threshold
# model.update_threshold(200)
#
# # retrieve cluster labels
# labels = model.get_cluster_labels()
#
# # retrieve metrics
# model.update_stats()
# tomato_stats = model.get_stats()
# print(tomato_stats)
#
# # removing non-clustered points
# locs = ~np.isnan(labels)
# points2 = points[locs,:]
# labels2 = labels[locs]
#
# plt.figure(figsize=(12,12))
# plt.scatter(points2[:,0], points2[:,1], c = labels2)
# figpath = os.path.join(cwd, 'plots/toy_example_with_clusters_from_computed_density.png')
# plt.savefig(figpath)
#
# ################################################################################
# ###################### Using a knn graph #######################################
#
# # testing knn Graph function
# G = KnnGraph(points, 60)
#
# density = data[:,-1]
#
# # Instantiate model with the provided density function
# model = ToMAToModel(G, density)
#
# # retrieve prominences
# prominences = model.get_mode_prominences()
# print(prominences[:10])
#
#
# #plot persistence diagram
# figpath = os.path.join(cwd, 'plots/toy_example_pd_diagram_with_knnGraph.png')
# model.generate_persistence_diagram(figpath)
#
# # update threshold
# model.update_threshold(1000)
#
# # retrieve cluster labels
# labels = model.get_cluster_labels()
#
#
# # removing non-clustered points
# locs = ~np.isnan(labels)
# points2 = points[locs,:]
# labels2 = labels[locs]
#
# plt.figure(figsize=(12,12))
# plt.scatter(points2[:,0], points2[:,1], c = labels2)
# figpath = os.path.join(cwd, 'plots/toy_example_with_clusters_using_knnGraph.png')
# plt.savefig(figpath)
#
# ################################################################################
# ################################################################################
# #######################  Spiral example #######################################
# # Retrieve the data
# filepath = os.path.join(cwd, 'tomato/data/spiral_w_density.txt')
# data = np.loadtxt(filepath)
# print('Data shape: ', data.shape)
#
# points= data[:,:-1]
# density = data[:,-1]
#
# plt.figure(figsize=(12,12))
# plt.scatter(points[:,0], points[:,1], alpha = 0.2)
# figpath = os.path.join(cwd, 'plots/spiral_example.png')
# plt.savefig(figpath)
#
# # testing Graph function
# G = RadiusGraph(points, 10)
#
# # Instantiate model with the provided density function
# model = ToMAToModel(G, density)
#
# # retrieve prominences
# prominences = model.get_mode_prominences()
# print(prominences[:10])
#
#
# #plot persistence diagram
# figpath = os.path.join(cwd, 'plots/spiral_example_pd_diagram.png')
# model.generate_persistence_diagram(figpath)
#
# # update threshold
# model.update_threshold(1e-3)
#
# # retrieve cluster labels
# labels = model.get_cluster_labels()
#
# # removing non-clustered points
# locs = ~np.isnan(labels)
# points2 = points[locs,:]
# labels2 = labels[locs]
#
# plt.figure(figsize=(12,12))
# plt.scatter(points2[:,0], points2[:,1], c = labels2)
# figpath = os.path.join(cwd, 'plots/spiral_example_with_clusters.png')
# plt.savefig(figpath)

################################################################################
################################################################################
################    4 rings example    #########################################

# Retrieve the data
filepath = os.path.join(cwd,'tomato/data/4_rings_w_density.txt')
data = np.loadtxt(filepath)

print('Data shape: ', data.shape)

points= data[:,:-1]
density = data[:,-1]   # included density function

print(points.shape)

# The included density function gives weird answers.
# Computing instead an approximate density function using the inverse distance to measure

density2 = invDistMeasure(points, 60)

density2 = 100*density2 # scaling is optional

print(density2.shape)


fig = plt.figure(figsize=(20,20))
ax = fig.add_subplot(111, projection='3d')
ax.scatter(points[:,0], points[:,1], points[:,2], alpha = 0.01)
figpath = os.path.join(cwd, 'plots/4_rings.png')
fig.savefig(figpath)

#generating graph
print('Generating the neighborhood graph')
start = time.time()
G = RadiusGraph(points, 0.3)
end = time.time()
print("Duration: ", end-start)

################################################################################
# Instantiate model with the provided density function
print('Instantiate ToMATo model')
model = ToMAToModel(G, density2)

# retrieve prominences
prominences = model.get_mode_prominences()


print(prominences[:10])
#plot persistence diagram
figpath = os.path.join(cwd, 'plots/4_rings_pd_diagram.png')
model.generate_persistence_diagram(figpath)


# update threshold
model.update_threshold(300)

# retrieve cluster labels
labels = model.get_cluster_labels()

# retrieve metrics
model.update_stats()
tomato_stats = model.get_stats()
print(tomato_stats)


# gettting hierarchy tree and merge tree
DG1 = model.plot_tomato_hierarchy()
DG2 = model.plot_merge_tree()

print(list(DG1.edges))
print(list(DG2.edges))

# testing cluster_metrics
metrics = cluster_metrics(points, model)
print(metrics)

# removing non-clustered points
locs = ~np.isnan(labels)
points2 = points[locs,:]
labels2 = labels[locs]


plt.figure(figsize=(15,15))
ax = plt.subplot(projection="3d")
ax.scatter(points2[:,0], points2[:,1], points2[:,2], c= labels2, s= 0.01)
figpath = os.path.join(cwd, 'plots/4_rings_with_clusters_from_computed_density.png')
plt.savefig(figpath)
