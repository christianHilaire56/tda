import numpy as np
from sklearn.neighbors import NearestNeighbors


def predict_cluster(new_points, model_points, model):
    nnn = 30
    n_clusters = model.n_clusters
    nbrs = NearestNeighbors(n_neighbors=nnn, algorithm='ball_tree').fit(model_points)
    distances, indices = nbrs.kneighbors(new_points)
    # Retrieve labels
    labels = np.reshape(model.cluster_labels[indices.ravel()], (len(new_points), nnn))
    print(labels.shape)
    if model.graph_type == 'radius_graph':
        bandwidth = model.graph_radius
    elif model.graph_type == 'knn_graph':
        bandwidth = model.graph_app_radius
    K = np.exp(-0.5*distances**2/bandwidth)*(distances < bandwidth)
    Z = np.sum(K, axis = 1)  # normalizing term
    Z = Z + 1*(Z == 0)  # to take care of the case where Z is zero
    Probs = np.zeros((len(new_points),n_clusters+1))
    for i in range(n_clusters):
        Probs[:,i] = np.sum(K*(labels == i), axis = 1)/Z
    # Computing probability of being unclustered
    Probs[:,-1] = 1-np.sum(Probs, axis =1)
    # Determine most probable cluster
    predicted_labels = 1.0*np.argmax(Probs, axis =1)
    nan_locs = np.where(predicted_labels == n_clusters)[0]
    predicted_labels[nan_locs] = np.nan
    return Probs, predicted_labels
