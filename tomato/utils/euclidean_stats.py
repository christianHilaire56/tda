from __future__ import print_function
from scipy.spatial import distance_matrix
from sklearn import manifold
import numpy as np
import math


def cluster_metrics(points, model):
    n_clusters = model.n_clusters
    metrics={}
    metrics['average_distance_from_centroids']=[]
    metrics['max_distance_from_centroids']=[]
    metrics['distances_between_centroids']=[]
    reps = model.cluster_reps
    centroids = points[reps,:]
    metrics['distances_between_centroids'] = distance_matrix(centroids, centroids)
    for i in range(n_clusters):
        c = centroids[i,:]
        indices = np.where(model.cluster_labels==i)[0]
        cluster_points = points[indices,:]
        distances = distance_matrix(c[None, :], cluster_points)
        metrics['average_distance_from_centroids'].append(np.mean(distances))
        metrics['max_distance_from_centroids'].append(np.amax(distances))
    # Executing multidimensional scaling (needed to plot tree)
    mds = manifold.MDS(n_components=1, dissimilarity="precomputed", random_state=6)
    mds_results = mds.fit(metrics['distances_between_centroids'])
    metrics['mds_coords'] = mds_results.embedding_
    return metrics
