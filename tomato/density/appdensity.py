from __future__ import print_function
from sklearn.neighbors import NearestNeighbors
import numpy as np
import math




def invDistMeasure(points, nnn):
    # Inverse distance to measure function
    # points = numpy array of points
    # nnn = number of nearest neighbors
    nbrs = NearestNeighbors(n_neighbors=nnn+1, algorithm='ball_tree').fit(points)
    distances, indices = nbrs.kneighbors(points)
    return  1.0/np.sqrt(np.mean(distances[:,1:]**2, axis=1))


def truncatedGaussian(points, bandwidth=None):
    # A version of the truncated Gaussian estimator
    n_total = min(len(points)-1, 100)
    if bandwidth==None:
        d = points.shape[1]
        S_avg = np.mean(np.std(points[:n_total,:], axis =0))
        bandwidth = S_avg*math.pow(4/(n_total*(d+4)),1/(d+6))
    nbrs = NearestNeighbors(n_neighbors=n_total+1, algorithm='ball_tree').fit(points)
    distances, indices = nbrs.kneighbors(points)
    K = distances[:,1:]*(distances[:,1:] <= bandwidth)
    K = np.exp(-K**2/(2*bandwidth))
    return np.mean(K, axis = 1)
