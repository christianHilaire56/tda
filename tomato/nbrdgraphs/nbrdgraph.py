from __future__ import print_function
import numpy as np
from scipy.spatial import cKDTree
from sklearn.neighbors import NearestNeighbors


class NbrdGraph(object):

    def __init__(self, points, type):
        # points := numpy array of points that are supposed to be clustered
        self.n_nodes = len(points)
        self.type = type
        return

    def get_upperstar(self, i, mapping, reverse_mapping):
        local_index = mapping[i]
        neighbors = self.neighbors_list[local_index]
        neighbors = np.array([reverse_mapping[j] for j in neighbors])
        locs = np.where(neighbors < i)[0]
        return neighbors[locs]


################################################################################

class RadiusGraph(NbrdGraph):

    def __init__(self, points, radius):
        # points := numpy array of points that are supposed to be clustered
        # radius := radius of the neighborhood graph
        super(RadiusGraph, self).__init__(points=points, type='radius_graph')
        tree = cKDTree(points)
        self.neighbors_list = tree.query_ball_tree(tree, radius,  p = 2, eps = 0.0)
        self.radius = radius
        return

################################################################################

class KnnGraph(NbrdGraph):

    def __init__(self, points, nnn):
        # points := numpy array of points that are supposed to be clustered
        # nnn := number of nearest neighbors
        super(KnnGraph, self).__init__(points=points, type='knn_graph')
        nbrs = NearestNeighbors(n_neighbors=nnn+1, algorithm='ball_tree').fit(points)
        distances, indices = nbrs.kneighbors(points)
        self.neighbors_list = indices[:,1:]
        self.distances_list = distances[:,1:]
        self.nnn = nnn
        return
