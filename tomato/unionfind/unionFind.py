from __future__ import print_function
import array as arr




class UF(object):
    """Custom Union-Find data structure for the ToMATo algorithm"""

    def __init__(self, n_nodes):
        # n_nodes = number of nodes in the neighborhood graph
        self.reprs = arr.array('i', [i for i in range(n_nodes)]) # array of representatives
        return

    def find_reprs(self, x):
        # Find representative of x.  Function uses path compression"
        if x != self.reprs[x]:
            self.reprs[x]  = self.find_reprs(self.reprs[x])
        return self.reprs[x]

    def merge(self, r1, r2):
        # Merge cluster represented by r1 into cluster represented by r2
        self.reprs[r1] = r2
        return
