from __future__ import print_function
import matplotlib.pyplot as plt
import numpy as np


def plot_pddiagrams(persistences, figpath, display = False):
    # function to plot persistence diagram
    # persisenctes := dictionary containing persistence values (i.e (birth, death) pairs)
    births = []
    deaths = []
    for k in persistences:
        births.append(persistences[k][0])
        deaths.append(persistences[k][1])
    births = np.array(births)
    deaths = np.array(deaths)
    max_birth = np.amax(births)
    min_birth = np.amin(births)
    deaths[deaths==0] = min_birth/2.0
    plt.figure(figsize=(12,12))
    plt.plot([min_birth/2.0, max_birth], [min_birth/2.0, max_birth], c= 'red')
    plt.scatter(births, deaths)
    plt.axhline(y = min_birth/2.0,c= 'blue',linestyle='--')
    plt.xlim((min_birth/2.0,1.05*max_birth))
    plt.savefig(figpath)
    if display:
        plt.show()
    return

    
