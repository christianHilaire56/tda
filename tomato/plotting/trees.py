import networkx as nx
import numpy as np


def tomato_hierarchy(cluster_stats):
    DG = nx.DiGraph()
    n_clusters = cluster_stats['number_of_clusters']
    # adding the nodes
    DG.add_nodes_from([i for i in range(n_clusters)])
    # max_density = max(model.cluster_stats['density_of_centroid'] )
    # adding weighted edges
    for entry in cluster_stats['hierarchy_edge_list']:
        v1, v2 = entry
        DG.add_edge(v1,v2)
    return DG

def merge_tree(cluster_stats):
    DG = nx.DiGraph()
    n_clusters = cluster_stats['number_of_clusters']
    # adding the nodes
    DG.add_nodes_from([i for i in range(n_clusters)])
    # max_density = max(model.cluster_stats['density_of_centroid'] )
    # adding weighted edges
    for entry in cluster_stats['merge_edge_list']:
        v1, v2 = entry
        DG.add_edge(v1,v2)
    return DG
