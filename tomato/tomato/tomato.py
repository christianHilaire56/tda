from __future__ import print_function
from ..unionfind import UF
import numpy as np
from ..plotting import plot_pddiagrams, tomato_hierarchy, merge_tree
import os
import copy


def mainToMATo(get_upperstar, app_density, tau):
    # get_upperstar = function to retrieve the upper star neighbors of a node in the neighborhood graph as a numpy array
    # app_density = array of approximate density function values
    # tau = threshold for the prominences
    # NOTICE: The function assumes that the nodes are ordered in decreasing order
    #         of density function values

    n_nodes = len(app_density)
    # Instantiate union-find data structure
    U = UF(n_nodes)

    persistences = {}  # for storing (birth, death) values
    hierarchy = {} # another union-find type of data structure to keep track of merging
    for i in range(n_nodes):
        upper_star = get_upperstar(i)
        if len(upper_star) == 0:
            # the node is a local max
            persistences[i] = np.zeros(2)
            persistences[i][0] = app_density[i] # birth of a component
        else:
            gradient_node = np.amin(upper_star)
            r_gradient = U.find_reprs(gradient_node)
            U.merge(i, r_gradient)
            # merge clusters if necessary
            history =[] # keeping track of old cluster centers
            for j in upper_star:
                r_neighbor = U.find_reprs(j)
                r_current = U.find_reprs(i)
                if (r_neighbor != r_current):
                    r_high = min(r_neighbor, r_current)
                    r_low = max(r_neighbor, r_current)
                    min_prominence = app_density[r_low] - app_density[i]
                    if (min_prominence < tau):
                        U.merge(r_low, r_high)
                        persistences[r_low][1] = app_density[i]
                        history.append(r_low)
            for r in history:
                hierarchy[r] = r_high
    # Assign cluster labels
    cluster_labels = np.zeros(n_nodes)
    cluster_reps = []  # keeping track of the centroid for each cluster
    counter = 0
    for i in range(n_nodes):
        r_current = U.find_reprs(i)
        if i == r_current: # potential new label
            if app_density[i] > tau:
                cluster_labels[i] = counter
                cluster_reps.append(r_current)
                counter += 1
            else:
                cluster_labels[i] = np.nan
        else:
            cluster_labels[i] = cluster_labels[r_current]

    n_clusters = counter
    return cluster_labels, cluster_reps, persistences, hierarchy, n_clusters




class ToMAToModel(object):

    def __init__(self, G, app_density):
        # G := graph data structure
        # app_density := numpy array of density function values

        # find order to sort points
        order = np.argsort(-app_density)
        self.n_points = len(app_density)
        self.sorted_density = app_density[order]
        self.mapping={}
        self.reverse_mapping={}
        for i in range(self.n_points):
            self.mapping[i] = order[i]
            self.reverse_mapping[order[i]] = i
        self.get_upperstar = lambda i: G.get_upperstar(i, self.mapping, self.reverse_mapping)
        self.graph_type=G.type
        if self.graph_type == 'radius_graph':
            self.graph_radius = G.radius
        elif self.graph_type == 'knn_graph':
            self.graph_app_radius = np.mean(G.distances_list)
        # retrieve prominence values
        tau = 2*self.sorted_density[0]
        _, _, persistences, hierarchy, _  = mainToMATo(self.get_upperstar, self.sorted_density, tau)
        self.persistences = {}
        self.hierarchy = {}
        for key in persistences:
            new_key = self.mapping[key]
            self.persistences[new_key] = persistences[key]

        for key in hierarchy:
            new_key = self.mapping[key]
            self.hierarchy[new_key] = self.mapping[hierarchy[key]]

        self.cluster_labels = np.array([])
        self.n_clusters = 0
        self.cluster_reps = []
        self.cluster_stats = None
        return


    def update_threshold(self, tau):
        # tau := threshold for the prominences
        cluster_labels, cluster_reps, _ , _ , self.n_clusters = mainToMATo(self.get_upperstar, self.sorted_density, tau)
        self.cluster_labels = np.zeros(self.n_points)
        for i in range(self.n_points):
            self.cluster_labels[self.mapping[i]] = cluster_labels[i]
        self.cluster_reps=[self.mapping[cluster_reps[i]] for i in range(self.n_clusters)]
        print('The number of clusters: ', self.n_clusters)
        return


    def get_cluster_labels(self):
        return self.cluster_labels

    def get_mode_prominences(self):
        prominences = []
        for key in self.persistences:
            prominences.append(self.persistences[key][0] - self.persistences[key][1])
        prominences.sort(reverse=True)
        return prominences

    def generate_persistence_diagram(self, figpath = None, display = False):
        if figpath == None:
            cwd = os.getcwd()
            figpath = os.path.join(cwd, 'pd_diagram.png')
        plot_pddiagrams(self.persistences, figpath, display)
        return

    def get_merge_order(self):
        edges_list = []
        if self.n_clusters > 0:
            prominences = self.get_mode_prominences()
            top_prominences = prominences[:self.n_clusters]
        for i in range(self.n_clusters-1,0, -1):
            rep = self.cluster_reps[i]
            cur_prominence = self.persistences[rep][0] - self.persistences[rep][1]
            idx = top_prominences.index(cur_prominence)
            if idx > 0:
                diff = prominences[idx] - prominences[idx-1]
                tau = prominences[idx-1] + diff/2
                temp_cluster_labels, temp_cluster_reps, _ , _ , n_clusters = mainToMATo(self.get_upperstar, self.sorted_density, tau)
                print(n_clusters)
                temp_label = temp_cluster_labels[self.reverse_mapping[rep]]
                if not(np.isnan(temp_label)):
                    parent = self.mapping[temp_cluster_reps[int(temp_label)]]
                    edges_list.append([i, self.cluster_labels[parent]])
        return edges_list


    def update_stats(self):
        # provides stats about the clusters
        self.cluster_stats ={}
        self.cluster_stats['number_of_clusters'] = self.n_clusters
        self.cluster_stats['indices_of_centroids'] = copy.deepcopy(self.cluster_reps)
        self.cluster_stats['density_of_centroids'] = []
        self.cluster_stats['prominences_of_clusters'] = []
        self.cluster_stats['proportions_of_clusters'] = []
        self.cluster_stats['proportion_of_unclustered_points'] = np.sum(np.isnan(self.cluster_labels))/self.n_points
        self.cluster_stats['hierarchy_edge_list'] = []
        for i in range(self.n_clusters):
            proportion = len(np.where(self.cluster_labels==i)[0])/self.n_points
            self.cluster_stats['proportions_of_clusters'].append(proportion)
            rep = self.cluster_reps[i]
            self.cluster_stats['density_of_centroids'].append(self.sorted_density[self.reverse_mapping[rep]])
            prominence = self.persistences[rep][0] - self.persistences[rep][1]
            self.cluster_stats['prominences_of_clusters'].append(prominence)
            if rep in self.hierarchy:
                parent = self.hierarchy[rep]
                self.cluster_stats['hierarchy_edge_list'].append([i, int(self.cluster_labels[parent])])
        self.cluster_stats['merge_edge_list'] = self.get_merge_order()
        return

    def get_stats(self):
        return copy.deepcopy(self.cluster_stats)

    def plot_tomato_hierarchy(self):
        if self.cluster_stats == None:
            self.update_stats()
        DG = tomato_hierarchy(self.cluster_stats)
        return DG

    def plot_merge_tree(self):
        if self.cluster_stats == None:
            self.update_stats()
        DG = merge_tree(self.cluster_stats)
        return DG
